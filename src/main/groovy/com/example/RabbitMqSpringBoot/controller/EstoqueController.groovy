package com.example.RabbitMqSpringBoot.controller


import com.example.RabbitMqSpringBoot.service.RabbitMqService
import contantes.RabbitMqConstantes
import entities.EstoqueDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping(path = "estoque")
class EstoqueController {


    @Autowired
    RabbitMqService rabbitMqService

    @PutMapping
    private ResponseEntity alteraEstoque(@RequestBody EstoqueDto estoqueDto) {
        rabbitMqService.enviarMensagem(RabbitMqConstantes.FILA_STOQUE, estoqueDto)
        return new ResponseEntity(HttpStatus.OK)
    }

    @GetMapping
    private ResponseEntity buscaEstoque() {
        return new ResponseEntity(HttpStatus.OK)
    }
}
