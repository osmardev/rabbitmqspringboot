package com.example.RabbitMqSpringBoot.controller


import com.example.RabbitMqSpringBoot.service.RabbitMqService
import contantes.RabbitMqConstantes
import entities.PrecoDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping(value = "preco")
class PrecoController {


    @Autowired
    RabbitMqService rabbitMqService

    @PutMapping
    private ResponseEntity alteraPreco(@RequestBody PrecoDto precoDto){
        println precoDto.codigoProduto
        rabbitMqService.enviarMensagem(RabbitMqConstantes.FILA_PRECO, precoDto)
        return new ResponseEntity(HttpStatus.OK)

    }


}
