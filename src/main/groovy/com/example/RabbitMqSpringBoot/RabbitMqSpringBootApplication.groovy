package com.example.RabbitMqSpringBoot

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class RabbitMqSpringBootApplication {

	static void main(String[] args) {
		SpringApplication.run(RabbitMqSpringBootApplication, args)
	}

}
