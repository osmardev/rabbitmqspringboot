package com.example.RabbitMqSpringBoot.connections

import contantes.RabbitMqConstantes
import org.springframework.amqp.core.AmqpAdmin
import org.springframework.amqp.core.Binding
import org.springframework.amqp.core.DirectExchange
import org.springframework.amqp.core.Queue
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct

@Component
class RabbitMqConnection {

    private static final String NOME_EXCHANGE = "amq.direct"

    private AmqpAdmin amqpAdmin

    RabbitMqConnection(AmqpAdmin amqpAdmin) {
        this.amqpAdmin = amqpAdmin
    }

    private Queue fila(String nomeFila){
        return new Queue(nomeFila, true, false, false)
    }

    private DirectExchange trocaDireta(){
        return new DirectExchange(NOME_EXCHANGE)
    }

    private Binding relacionamento(Queue fila, DirectExchange troca){
        return new Binding(fila.getName(), Binding.DestinationType.QUEUE, troca.getName(), fila.getName(), null)
    }

    @PostConstruct
    private void adiciona(){
        /** criando as filas no java **/
        Queue filaEstoque = this.fila(RabbitMqConstantes.FILA_STOQUE)
        Queue filaPreco = this.fila(RabbitMqConstantes.FILA_PRECO)

        /** criando a exchenge **/
        DirectExchange troca = this.trocaDireta()

        /** criando a ligação da fila com a exchenge */
        Binding ligacaoEstoque = this.relacionamento(filaEstoque, troca)
        Binding ligacaoPreco = this.relacionamento(filaPreco, troca)

        /** Criando as filas no RabbitMq  */
        this.amqpAdmin.declareQueue(filaEstoque)
        this.amqpAdmin.declareQueue(filaPreco)

        /** declara uma exchenge */
        this.amqpAdmin.declareExchange(troca)

        /** declara um binding **/
        this.amqpAdmin.declareBinding(ligacaoEstoque)
        this.amqpAdmin.declareBinding(ligacaoPreco)

    }

}
