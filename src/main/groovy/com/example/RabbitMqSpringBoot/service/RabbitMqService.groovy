package com.example.RabbitMqSpringBoot.service

import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class RabbitMqService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    void enviarMensagem(String nomeFila, Object mensagem){
        this.rabbitTemplate.convertAndSend(nomeFila, mensagem)
    }
}
